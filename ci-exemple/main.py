import os

import consolemenu.prompt_utils
from consolemenu import *
from consolemenu.items import *

from utils.utils import get_system_info

_version = 1
_console = consolemenu.PromptUtils(Screen())
LINE = 32
DEFAULT_INFO_FILE = 'sys_info.txt'


def clear():
    """Ordre al terminal d'un SO per esborrar la pantalla
            nt: Windows
            posix: GNU/Linux
    """
    # L'interpret de python emmagatzema l'últim valor de cada execució a la variable especial _
    _ = os.system('cls') if os.name == 'nt' else os.system('clear')


def main_menu_title():
    """ Retorna el text del menu principal """
    global _version
    return f'Demo project 2022 - 2023 v{_version}'


def main_menu_subtitle():
    """ Retorna el text del submenú """
    global _version
    return f'Opcions v{_version}'


def increment_version():
    global _version
    _version += 1


def decrement_version():
    global _version
    _version -= 1


def show_system_info():
    print("*" * LINE)
    print(get_system_info())
    print("*" * LINE)
    _console.enter_to_continue(message="\nPolsa una tecla per continuar")


def surt(missatge, wait=False):
    print(f'{missatge}')
    if wait:
        _console.enter_to_continue(message="\nPolsa una tecla per continuar")

    exit(0)


def main():
    clear()  # Esborrem la pantalla

    # Creem el menu principal on passarem les funcions com arguments
    menu = ConsoleMenu(main_menu_title(), main_menu_subtitle())

    # Elements de menú estàtics
    item1 = FunctionItem("Obtenir informació del sistema", show_system_info)
    item2 = FunctionItem("Surt", surt, ['Fins la propera', False])

    # Add all the items to the root menu
    menu.append_item(item1)
    menu.append_item(item2)

    # Show the menu
    menu.start(show_exit_option=False)
    menu.join()


if __name__ == "__main__":
    main()
