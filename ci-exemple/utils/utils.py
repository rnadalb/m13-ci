from platform import uname


def get_system_info():
    system = uname()
    return f"""System: {system.system}\nNode Name: {system.node}\nRelease: {system.release}\nMachine: {system.machine}\nProcessor: {system.processor}"""