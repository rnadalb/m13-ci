# M13 Continuous Integration

Projecte bàsic d'exemple per introduir la integració contínua

La integració contínua (CI) és la pràctica d'automatitzar la integració dels canvis de codi de diversos contribuïdors en un únic projecte de programari. És una de les principals pràctiques recomanades de DevOps, que permet als desenvolupadors fusionar amb freqüència els canvis de codi en un repositori central on després s'executen les compilacions i proves. Les eines automatitzades serveixen per a verificar que el nou codi és correcte abans de la integració.

Un sistema de control de versions del codi font és el punt clau del procés de CI. El sistema de control de versions també es complementa amb altres comprovacions com les proves automatitzades de qualitat del codi, les eines de revisió d'estil de sintaxi i molt més.


En aquest exemple bàsic i elemental l'únic que provarem és la contrucció de l'aplicació (build) i el linting proporcionat per [flake8](https://flake8.pycqa.org/en/latest/).

La fase de **build** simplement crea l'entorn d'execució tal i com ho fariem al client.
La fase de **linting** utitliza la llibreria *flake8* per comprovar si el codi que hem generat segueix  criteris de qualitat de codi.

Tal i com podem llegir a l'article "*[Escribiendo código de alta calidad en Python](https://medium.com/@gonzaloandres.diaz/fundamentos-y-automatizacion-codigo-de-alta-calidad-en-python-2020-671706a7f09b)*":

- La llegibilitat del codi font està relacionat amb el seu manteniment.
- El 70% del cost total d'un producte de programari és el seu manteniment.
- La llegibilitat de codi font i de la documentació és fonamental per al seu manteniment.
- Llegir el codi és el component més lent de totes les activitats de manteniment.
- Al llarg de les fases d'inspecció de codi s'ha de verificar la seva llegibilitat.
- La llegibilitat garanteix el manteniment, la portabilitat i la reutilització de codi.


## Informació interessant sobre gitLab CI

- [ ] [GitLab CI/CD variables](https://docs.gitlab.com/ee/ci/variables/)
- [ ] [Add a CI/CD variable to a projec](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
- [ ] [Add a CI/CD variable to a group](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-group)
- [ ] [Add a CI/CD variable to an instance](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-an-instance)

## Review

Revisa el fitxer [.gitlab-ci.yml](.gitlab-ci.yml) del repositori. És un exemple molt bàsic però t'ajudarà a entendre el procés 😉

## Project status

Updating...
